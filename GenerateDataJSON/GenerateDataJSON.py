# -*- coding: utf-8 -*-
"""
Created on Sat Apr 15 10:54:18 2017

@author: Nguyen Tran
This is a tool for parsing dataset into the format that can be understood
and utilized by WSP as sensor data for replaying
"""
"""
Abstract class representing all types of generator utilities 
"""
class AbsGenerator(object):
    def __init__(self, inputPaths, outputPath, *args, **kwargs):
        self.inputPaths = inputPaths
        self.outputPath = outputPath
        
    def process(self, *args):
        pass
    
    def generateOutput(self, *args):
        pass
    
    def readInput(self, *args):
        pass
    
    def writeOutput(self, *args):
        pass

"""
Abstract class representing a generator utility that generate JSON files
"""
class AbsJSONGenerator(AbsGenerator):
    def __init__(self, inputPaths, outputPath, *args, **kwargs):
        super(AbsJSONGenerator, self).__init__(inputPaths, outputPath, *args, **kwargs)
        
    def writeOutput(self, outputData):
        print("==================================")
        print("Start writing to file")
        import json
        import io
        try:
            to_unicode = unicode
        except NameError:
            to_unicode = str
            
        with io.open(self.outputPath, "w", encoding = "utf8") as outfile:
            str_ = json.dumps(outputData, indent=4, sort_keys=True, separators=(",", ": "), ensure_ascii=False)
            outfile.write(to_unicode(str_))
        print("Finish writing")
        print("==================================")
         
        
"""
Abstract class representing a generator utility that generates CSV files
One CSV file for each datastream
"""
class AbsCSVGenerator(AbsGenerator):
    def __init__(self, inputPaths, outputPath, *args, **kwargs):
        super(AbsCSVGenerator, self).__init__(inputPaths, outputPath, *args, **kwargs)
        # Storing the file handler for each data stream
        self.dataStreamsFiles = {}
        
    def writeOutput(self, outputData, streamID, fileHandler):        
        path = "%s/%s.csv" % (self.outputPath, streamID)
        if not(fileHandler):
            f = open(path, "w")
            print("Created a new file handler")
        else:
            f = fileHandler
            print("Reuse existing file handler")
            
        f.write("%s\n" % outputData)
        
        print("Wrote %s to %s" % (outputData, path))
        return f
        
    def closeFileHandlers(self):
        for key in self.dataStreamsFiles:
            self.dataStreamsFiles[key].close()
        
"""
Process data from the input text file of IntelLab line by line and generate CSS
files that WSP can understand. Each stream has one css file
"""
class IntelLabCSVDataGenerator(AbsCSVGenerator):
    def __init__(self, inputPaths, outputPath):
        super(IntelLabCSVDataGenerator, self).__init__(inputPaths, outputPath)
    
    def process(self):       
        print("Hello from process()")
        
        line, f = self.readInput(self.inputPaths, None)
        outputData = self.generateOutput(line)

        while True:
            line, f = self.readInput(self.inputPaths, f)
            if not(line == ""):
                outputData = self.generateOutput(line)
                for key in outputData:
                    if key in self.dataStreamsFiles:
                        self.writeOutput(outputData[key], key, self.dataStreamsFiles[key])
                    else:
                        fileWriteHandler = self.writeOutput(outputData[key], key, None)
                        self.dataStreamsFiles[key] = fileWriteHandler
            else:
                print("Reached the end of file!")
                self.closeFileHandlers()
                print("All file handlers closed")
                break
    
    def readInput(self, inputPath, fileHandler):
        import io
        if not(fileHandler):
            f = io.open(inputPath, "r") 
        else:
            f = fileHandler
        
#        Read a line from the input file
        line = f.readline()
        if line == "":
            f.close()
            return "", None
        else:
            return line, f
        
    def generateOutput(self, line):        
        def appendOutputData(outputData, streamID, date, time, result):
            try:
                datetime = date.replace(":", "") + "T" + time.replace(":", "").replace(".", "")
                outputData[streamID] = "%s %s" % (datetime, float(result))
            except ValueError:
                print("Result of %s -- %s is invalid." % (date, time))
                pass
        
        date, time, epoch, moteid, temperature, humidity, light, voltage = line.split(" ")
        outputData = {}
        try:
            senID = int(moteid)
        except ValueError:
            print("A sensor ID is invalid: %s" % moteid)
            return outputData
    
        #append observation info to the output data to be written to CSV files
        appendOutputData(outputData, "%s-temperature" % senID, date, time, temperature)
        appendOutputData(outputData, "%s-humidity" % senID, date, time, humidity)
        appendOutputData(outputData, "%s-light" % senID, date, time, light)
        appendOutputData(outputData, "%s-voltage" % senID, date, time, voltage)
        return outputData
        
        
"""
Process data from the input text file of IntelLab line by line and generate JSON
Only usable for small datasets
"""
class IntelLabJSONGenerator(AbsJSONGenerator):
    def __init__(self, inputPaths, outputPath):
        super(IntelLabJSONGenerator, self).__init__(inputPaths, outputPath)
        self.outputData = {}
        
    def process(self):      
        print("Hello from process()")
        
        line, f = self.readInput(self.inputPaths, None)
        self.generateOutput(line)
        
        while True:
            line, f = self.readInput(self.inputPaths, f)
            if not(line == ""):
                self.generateOutput(line)
            else:
                print("Reached the end of file!")
                break
            
        self.writeOutput(self.outputData)
                
        
        
    def readInput(self, inputPath, fileHandler):
        import io
        if not(fileHandler):
            f = io.open(inputPath, "r") 
        else:
            f = fileHandler
        
#        Read a line from the input file
        line = f.readline()
        if line == "":
            f.close()
            return "", None
        else:
            return line, f
        
        
    def generateOutput(self, line):
        def appendObservation(ls, date, time, result):
            try:
                ls.append({
                        "date" : date,
                        "time" : time,
                        "result" : float(result.strip())
                        })
            except ValueError:
                print("Result of %s -- %s is invalid." % (date, time))
                pass
        
        date, time, epoch, moteid, temperature, humidity, light, voltage = line.split(" ")
#        print(line.split(" ")[3])
        try:    
            senID = int(moteid)
            if not(senID in self.outputData):
                self.outputData[senID] = {
                        "%s-temperature" % senID : [],
                        "%s-humidity" % senID : [],
                        "%s-light" % senID : [],
                        "%s-voltage" % senID : []
                        }
                
            appendObservation(self.outputData[senID]["%s-temperature" % senID], date, time, temperature)
            appendObservation(self.outputData[senID]["%s-humidity" % senID], date, time, humidity)
            appendObservation(self.outputData[senID]["%s-light" % senID], date, time, light)
            appendObservation(self.outputData[senID]["%s-voltage" % senID], date, time, voltage)
        except ValueError:
            print("A sensor ID is invalid: %s" % moteid)
            pass
        
        


if __name__ == "__main__":
    generator = IntelLabCSVDataGenerator("./IntelLab/data.txt", "./IntelLab/data")
    generator.process()