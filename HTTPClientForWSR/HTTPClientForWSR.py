# -*- coding: utf-8 -*-
"""
Created on Sun Apr 16 12:27:23 2017

@author: Nguyen Tran
A simple HTTP client that sends JSON query to a WSR instance and displays the result in the console
"""
import httplib2 as http
import json
import timeit
import time
import Queue
import threading
import socket
import random

        
try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

def getResponseJSON(h, uri, path = '', method = 'POST', headers = {'Accept' : 'application/json', 'Content-Type' : 'application/json; charset = UTF-8'},
                        body = ''):
        target = urlparse(uri + path)
        response, content = h.request(target.geturl(), method, body, headers)
#        return json.loads(content)
        return content

def queryWSR(host, datum):
    h = http.Http()
    response = getResponseJSON(h, host, body = json.dumps(datum))
    return json.loads(response)

def testQuery():
    datum = (
            {"stream.unitOfMeasurement.name" : "degree Celsius"},
            {"FoI" : "FoI information"},
            {"Location" : "Location INformation!!"},
            {"Observation" : "Observation info!"},
            {"QoS" : "QoS, yo"}
            )
#    host = "http://129.127.230.65:9000"
    host = "http://localhost:9000"
#    host = "http://localhost:8000"
    response = queryWSR(host, datum)
    return response
    
def timeSingleQuery(n, q):
    try:
        avgTime = timeit.timeit(testQuery, number = n) / float(n)
    except socket.error as e:
        print e
        avgTime = -1
    q.put(avgTime)
    return avgTime
        

def timeMultipleQuerySparse(m, n):
    q = Queue.Queue()
    # Spawn threads
    count = 0
    while count < m - 5:
        spawnNum = int(random.uniform(0,5))
        for i in range(spawnNum):
            t = threading.Thread(target=timeSingleQuery, args=(n, q))
            t.start()
        print("Spawning %s clients" % spawnNum)
        count += spawnNum
        time.sleep(0.5)
    for i in range(m - count):
        t = threading.Thread(target=timeSingleQuery, args=(n, q))
        t.start()
    
    # Get avg runtime result from queue
    results = []
    for i in range(m):
        try:
            results.append(q.get(timeout=100))
        except:
            break
        
    # Count non error results
    count = 0
    for res in results:
        if not(res == -1):
            count += 1
    print(results)
    print(count)
    return results
    
def test():
    timeMultipleQuerySparse(200, 1)

    
if __name__ == "__main__":
    startTime = timeit.default_timer()
    test()
    elapsed = timeit.default_timer() - startTime
    print(elapsed)
#    print(timeit.timeit(_test, number = 1))
