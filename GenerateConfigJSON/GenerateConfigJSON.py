# -*- coding: utf-8 -*-
"""
Created on Sat Apr 15 16:44:40 2017

@author: Nguyen Tran

This utility script is for generating configuration and data model file quickly for WSP
"""

def generateModule(moduleName, modulePath, params, subModules):
    # Module parameters must be dictionary and subModules must be list
    if not(type(params) == dict and type(subModules) == list):
        raise
        
    return {"moduleName" : moduleName,
            "modulePath" : modulePath,
            "params" : params,
            "subModules" : subModules}

def generateSensorInfo(sensorID, sensorName, description, encodingType, metadata):
    return {"id" : sensorID,
            "name" : sensorName,
            "description" : description,
            "encodingType" : encodingType,
            "metadata" : metadata}
    
def generateStreamInfo(streamID, name, description, observationType, unitOfMeasurement,
                       observedArea, phenomenonTime, resultTime, observedProperty, FOI):
    return {
			"id" : streamID,
			"name" : name,
			"description" : description,
			"observationType" : observationType,
			"unitOfMeasurement" : unitOfMeasurement,
			"observedArea" : observedArea,
			"phenomenonTime" : phenomenonTime,
			"resultTime" : resultTime,
			"property" : observedProperty,
			"FOI" : FOI
		}
    
def generatePropertyInfo(propID, name, definition, description):
    return {
				"id" : propID,
				"name" : name,
				"definition" : definition,
				"description" : description
			}
    
def generateFOI(FOIID, name, description, encodingType, feature):
    return {
				"id" : FOIID,
				"name" : name,
				"description" : description,
				"encodingType" : encodingType,
				"feature" : feature
			}

def generateStreamConfig(streamID, plugin):
    if type(plugin) == list:
        raise
    return {
					"id" : streamID,
					"plugin" : plugin
				}
    
def generateSensorConfig(sensorID, streams):
    if not(type(streams) == list):
        raise
    
    return {
			"id" : sensorID,
			"streams" : streams
		}



def generateSystemConfigs():
    config = {"sensors" : [],
              "http" : {},
              "ws" : {}}
    dataModel = {"ModelManager" : {},
                 "Infos" :{}}
    
    # Create model manager module
    modelManager = generateModule("STModelManager", "./../../Models/SensorThing", {}, [])
    dataModel["ModelManager"] = modelManager
    
    # Create HTTP config module
    JSONView = generateModule("JSONView", "./../../Views", {}, [])
    routers = []
    routers.append(generateModule("RootRouter", "./../../Routers", {"route" : "/", "verb" : "GET"}, [JSONView]))
    routers.append(generateModule("NestedRouter", "./../../Routers", {"route" : "/:entityType\\(:id\\)/:prop", "verb" : "GET"}, [JSONView]))
    routers.append(generateModule("NestedRouter", "./../../Routers", {"route" : "/:entityType\\(:id\\)", "verb" : "GET"}, [JSONView]))
    routers.append(generateModule("NestedRouter", "./../../Routers", {"route" : "/:entityType", "verb" : "GET"}, [JSONView]))
    httpModule = generateModule("ExpressHTTPServer", "./../Servers", {
			"host" : "localhost",
			"port" : "8500",
			"version" : "1.0"
		}, routers)
    config["http"] = httpModule
    
    
    # Create sensor configs and model
    totalSensorNum = 54
    streamTypes = [{"streamType" : "temperature",
                    "observationType" : "OM_Measurement",
                    "unit" : "degree Celsius",
                    "obsPropID" : "Property 1",
                    "obsProp" : "Apparent Temperature"}, 
                    {"streamType" : "humidity",
                    "observationType" : "OM_Measurement",
                    "unit" : "percent",
                    "obsPropID" : "Property 2",
                    "obsProp" : "Humidity"},
                     {"streamType" : "light",
                    "observationType" : "OM_Measurement",
                    "unit" : "lux",
                    "obsPropID" : "Property 3",
                    "obsProp" : "Luminance"},
                    {"streamType" : "voltage",
                    "observationType" : "OM_Measurement",
                    "unit" : "volt",
                    "obsPropID" : "Property 4",
                    "obsProp" : "Voltage"}]
    period = 1000
    dataPath = "./Configs/generated_config_intel_lab/data"
    senID = 1
    while senID <= totalSensorNum:
        senName = "Sensor %s" % senID
        senDesc = "Sensor %s in the Intel Lab Dataset" % senID
        streams = []
        for strType in streamTypes:
            streamType = strType["streamType"]
            observationType = strType["observationType"]
            unit = strType["unit"]
            streamID = "%s-%s" % (senID, streamType)
            replayPlugin = generateModule("ReplayPlugin", "./../../Plugins", {
        							"path" : dataPath,
        							"streamID" : streamID,
        							"period" : period
        						}, [])
            # Generate stream configuration and data model
            stream = generateStreamConfig(streamID, replayPlugin)
            obsProperty = generatePropertyInfo(strType["obsPropID"], strType["obsProp"], strType["obsPropID"], "Description of Property 1")
            FOI = generateFOI("FOI1", "FOI 1", "FOI Type 1", "Encoding Type", "N/A")
            streamInfo = generateStreamInfo(streamID, "Temperature stream %s" % streamID,
                                            "This is a %s stream belonging to sensor %s" % (streamType, streamID),
                                            observationType, unit, "N/A", "N/A", "N/A", obsProperty, FOI)
            streams.append(stream)
            
            # Add stream data model to the data model of the whole system
            dataModel["Infos"][streamID] = streamInfo
            
        sensor = generateSensorConfig(senID, streams)
        sensorInfo = generateSensorInfo(senID, senName, senDesc, "PDF", "N/A")    
        
        config["sensors"].append(sensor)
        dataModel["Infos"][senID] = sensorInfo
        
        # Process next sensor
        senID += 1
    
    return config, dataModel
    

def writeOutput(outputData, outputPath):
    print("==================================")
    print("Start writing to file")
    import json
    import io
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str
        
    with io.open(outputPath, "w", encoding = "utf8") as outfile:
        str_ = json.dumps(outputData, indent=4, sort_keys=True, separators=(",", ": "), ensure_ascii=False)
        outfile.write(to_unicode(str_))
    print("Finish writing")
    print("==================================")


if __name__ == "__main__":
    config, dataModel = generateSystemConfigs()
    writeOutput(config, "./configuration.json")
    writeOutput(dataModel, "./data_model.json")
